
const mongoose = require("mongoose");
const CustomerModel = require('../model/modelCustomer')
// lấy tất cả ProductType
const getAllCustomer = (request, response) => {
    //b1: thu thập dữ liệu
    //b2: validate dữ liệu
    //b3: thao tác với cơ sở dữ liệu
    CustomerModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        }
        else {
            response.status(200).json({
                status: "Success: Get all Customer success",
                data: data
            })
        }
    })
}
// tạo mới producttype
const createCustomer = (request, response) => {
    //b1: thu thập dữ liệu
    let bodyRequest = request.body;
    console.log(bodyRequest)
    //b2: validate dữ liệu
    if (!bodyRequest.fullName || !bodyRequest.phone || !bodyRequest.email) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "not is require"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    let createCustomer = {
        _id: mongoose.Types.ObjectId(),
        fullName: bodyRequest.fullName,
        phone: bodyRequest.phone,
        email: bodyRequest.email,
        address: bodyRequest.address,
        city: bodyRequest.city,
        country: bodyRequest.country,
    }
    CustomerModel.create(createCustomer, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Create Customer success",
                data: data
            })
        }
    })
}
// lấy Cústomer theo id
const getCustomerById = (request, response) => {
    //b1: thu thập dữ liệu
    let CustomerId = request.params.CustomerId;
    //b2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(CustomerId)) {
        response.status(400).json({
            status: "Error 400: bad request",
            message: "Customer ID is not valid"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    CustomerModel.findById(CustomerId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        }
        else {
            response.status(200).json({
                status: "Success: Get Customer by id success",
                data: data
            })
        }
    })
}
// update theo id
const updateCustomertById = (request, response) => {

    //B1:Chuẩn bị dữ liệu
    let CustomerId = request.params.CustomerId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(CustomerId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order ID is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let CustomerUpdate = {
        fullName: bodyRequest.fullName,
        phone: bodyRequest.phone,
        email: bodyRequest.email,
        address: bodyRequest.address,
        city: bodyRequest.city,
        country: bodyRequest.country,
    }


    CustomerModel.findByIdAndUpdate(CustomerId, CustomerUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update Customer success",
                data: data
            })
        }
    })
}
// DELETE A  ProductType
const deleteCustomerId = (request, response) => {

    //B1: Chuẩn bị dữ liệu
    let CustomerId = request.params.CustomerId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(CustomerId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Customer ID is not a valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    CustomerModel.findByIdAndDelete(CustomerId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Delete Customer success"
            })
        }
    })
}
module.exports = {
    getAllCustomer:getAllCustomer,
    createCustomer:createCustomer,
    getCustomerById: getCustomerById,
    updateCustomertById : updateCustomertById,
    deleteCustomerId: deleteCustomerId
}